﻿using System;
using System.Collections;

namespace MyApp // Note: actual namespace depends on the project name.
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Create a list of strings.
            var footballPlayer = new List<string>();
            footballPlayer.Add("Ronaldo");
            footballPlayer.Add("Messi");
            footballPlayer.Add("Neymar");
            footballPlayer.Add("Mbappe");
            footballPlayer.Add("Benzema");

            // Iterate through the list.
            foreach (var player in footballPlayer)
            {
                Console.Write(player + " > ");
            }

            // add hashtable
            // Create a new hash table.
            //
            Hashtable eachPlayersClub = new Hashtable();

            // Add some elements to the hash table. There are no
            // duplicate keys, but some of the values are duplicates.
            eachPlayersClub.Add("Ronaldo",  "Sporting Lisbon, Manchester United, Real Madird, Juventus, Manchester United, Al Nasr");
            eachPlayersClub.Add("Messi", "Barcelona, PSG");
            eachPlayersClub.Add("Neymar", "Barcelone, PSG");
            eachPlayersClub.Add("Mbappe", "AS Monaco, PSG");
            eachPlayersClub.Add("Benzema", "Lyon, Real Madird");

            Console.WriteLine("");
            foreach (DictionaryEntry pc in eachPlayersClub )
            {
                Console.WriteLine("Key : {0}, value : {1}", pc.Key, pc.Value);
            }

            // add dictionary


        }
    }
}